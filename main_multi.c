/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main_multi.c
 * Author: mignotju
 *
 * Created on April 18, 2016, 12:02 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "fonction_classification.h"
#include "sortie_excel.h"

/*
 * 
 */
int main() {
    printf("Application du modèle Multinomial...\n \n");
    struct liste_documents list_docs;
    list_docs.taille = NOMBRE_DOCS;
    list_docs.docs = malloc(NOMBRE_DOCS * sizeof (struct document));

    parser_doc("BaseReuters-29", &list_docs);

    //Question 1
    long dimension_pb = trouve_dimension(&list_docs);
    printf("La dimension du problème est : %ld\n", dimension_pb);

    double* tab_classes = calloc(30, sizeof (double));
    nombre_docs_par_classe(tab_classes, &list_docs);

    //Question 2
    struct liste_documents list_docs_entrainement;
    struct liste_documents list_docs_test;

    list_docs_entrainement.taille = TAILLE_ENTRAINEMENT;
    list_docs_entrainement.docs = malloc(TAILLE_ENTRAINEMENT * sizeof (struct document));

    list_docs_test.taille = TAILLE_TEST;
    list_docs_test.docs = malloc(TAILLE_TEST * sizeof ( struct document));

    scinder(&list_docs, &list_docs_entrainement, &list_docs_test);

    //Question 3
    struct Param_Multinomial param_multi = entrainement_Multinomial(&list_docs_entrainement, dimension_pb);

    //Question 4
    long* tableau_prediction_multi = malloc(TAILLE_TEST * sizeof (long));
    calcul_argmax_multi(tableau_prediction_multi, param_multi, &list_docs_test);

    double taux_classification_multi = calcul_taux_classification(tableau_prediction_multi, &list_docs_test);
    printf("Le taux de bonne classification du 1er test est : %f\n \n", taux_classification_multi);

    //Question 5
    double* tab_taux = calloc(20, sizeof (double));
    printf("Réalisation de 20 autres tests pour obtenir un taux de bonne classification moyen... \n \n");
    for (int i = 0; i < 20; i++) {
        struct liste_documents list_docs_entrainement;
        struct liste_documents list_docs_test;

        list_docs_entrainement.taille = TAILLE_ENTRAINEMENT;
        list_docs_entrainement.docs = malloc(TAILLE_ENTRAINEMENT * sizeof (struct document));

        list_docs_test.taille = TAILLE_TEST;
        list_docs_test.docs = malloc(TAILLE_TEST * sizeof ( struct document));

        scinder(&list_docs, &list_docs_entrainement, &list_docs_test);

        struct Param_Multinomial param_multi = entrainement_Multinomial(&list_docs_entrainement, dimension_pb);

        long* tableau_prediction_multi = malloc(TAILLE_TEST * sizeof (long));
        calcul_argmax_multi(tableau_prediction_multi, param_multi, &list_docs_test);

        double taux_classification_multi = calcul_taux_classification(tableau_prediction_multi, &list_docs_test);

        tab_taux[i] = taux_classification_multi;
        printf("Le taux de bonne classification du test numero %i est : %f \n",i,taux_classification_multi);
    }

    ecriture_fichier_excel(tab_taux, "excel/taux_classification_multi.txt", 0, 0, 20);
    printf("Données sur les 20 tests pour exploitation sur excel créées.\n");

    for (int i = 0; i < list_docs.taille; i++) {
        free(list_docs.docs[i].mots);
    }
    free(list_docs.docs);
    free(tab_classes);
    free(list_docs_entrainement.docs);
    free(list_docs_test.docs);
    free(tab_taux);
    free(tableau_prediction_multi);
    free(param_multi.theta);
    free(param_multi.pi);

    return (EXIT_SUCCESS);
}

