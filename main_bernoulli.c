/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main_bernoulli.c
 * Author: mignotju
 *
 * Created on April 18, 2016, 12:05 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "fonction_classification.h"
#include "sortie_excel.h"

/*
 * 
 */
int main() {
    printf("Application du modèle de Bernoulli...\n \n");
    struct liste_documents list_docs;
    list_docs.taille = NOMBRE_DOCS;
    list_docs.docs = malloc(NOMBRE_DOCS * sizeof (struct document));

    parser_doc("BaseReuters-29", &list_docs);

    //Question 1
    long dimension_pb = trouve_dimension(&list_docs);
    printf("La dimension du problème est : %ld\n", dimension_pb);

    double* tab_classes = calloc(30, sizeof (double));
    nombre_docs_par_classe(tab_classes, &list_docs);

    //Question 2
    struct liste_documents list_docs_entrainement;
    struct liste_documents list_docs_test;

    list_docs_entrainement.taille = TAILLE_ENTRAINEMENT;
    list_docs_entrainement.docs = malloc(TAILLE_ENTRAINEMENT * sizeof (struct document));

    list_docs_test.taille = TAILLE_TEST;
    list_docs_test.docs = malloc(TAILLE_TEST * sizeof ( struct document));

    scinder(&list_docs, &list_docs_entrainement, &list_docs_test);

    //Question 3
    struct Param_Bernoulli param_ber = entrainement_Bernoulli(&list_docs_entrainement, dimension_pb);

    //Question 4
    long* tableau_prediction_bernoulli = malloc(TAILLE_TEST * sizeof (long));
    calcul_argmax_bernoulli(tableau_prediction_bernoulli, param_ber, &list_docs_test, dimension_pb);

    double taux_classification_bernoulli = calcul_taux_classification(tableau_prediction_bernoulli, &list_docs_test);
    printf("Le taux de bonne classification du 1er test est : %f\n", taux_classification_bernoulli);

    //Question 5
    double* tab_taux = calloc(20, sizeof (double));
    printf("Réalisation de 20 autres tests pour obtenir un taux de bonne classification moyen... \n \n");
    for (int i = 0; i < 20; i++) {
        struct liste_documents list_docs_entrainement;
        struct liste_documents list_docs_test;

        list_docs_entrainement.taille = TAILLE_ENTRAINEMENT;
        list_docs_entrainement.docs = malloc(TAILLE_ENTRAINEMENT * sizeof (struct document));

        list_docs_test.taille = TAILLE_TEST;
        list_docs_test.docs = malloc(TAILLE_TEST * sizeof ( struct document));

        scinder(&list_docs, &list_docs_entrainement, &list_docs_test);

        struct Param_Bernoulli param_ber = entrainement_Bernoulli(&list_docs_entrainement, dimension_pb);

        long* tableau_prediction_bernoulli = malloc(TAILLE_TEST * sizeof (long));
        calcul_argmax_bernoulli(tableau_prediction_bernoulli, param_ber, &list_docs_test, dimension_pb);

        double taux_classification_bernoulli = calcul_taux_classification(tableau_prediction_bernoulli, &list_docs_test);

        tab_taux[i] = taux_classification_bernoulli;
        printf("Le taux de bonne classification du test numero %i est : %f \n",i,taux_classification_bernoulli);
    }

    ecriture_fichier_excel(tab_taux, "excel/taux_classification_bernoulli.txt", 0, 0, 20);
    printf("Données sur les 20 tests pour exploitation sur excel créées.\n");

    free(list_docs.docs);
    free(tab_classes);
    free(list_docs_entrainement.docs);
    free(list_docs_test.docs);
    free(tab_taux);
    free(tableau_prediction_bernoulli);
    free(param_ber.theta);
    free(param_ber.pi);

    return (EXIT_SUCCESS);
}

