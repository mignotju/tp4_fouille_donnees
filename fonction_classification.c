/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "fonction_classification.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

struct Param_Multinomial entrainement_Multinomial(struct liste_documents* list_entr, long dimension_pb) {
    struct Param_Multinomial multi;

    //nb_classe[k]= nb de docs de la liste de docs de l'entrainement dans le classe k
    double* nb_classe = calloc(30, sizeof (double));
    nombre_docs_par_classe(nb_classe, list_entr);
    double* pi = malloc(30 * sizeof (double));
    for (long k = 1; k < 30; k++) {
        pi[k] = (double) nb_classe[k] / (double) TAILLE_ENTRAINEMENT;
    }

    double** theta;
    theta = calloc(30, sizeof (double*));
    //nb_tot_k[k]=nb tot de terme dans classe k
    double *nb_tot_k = calloc(30, sizeof (double));
    //nb_occ_ik[k][i]=nb de terme ti dans classe k
    double **nb_occ_ki = calloc(30, sizeof (double*));

    for (int k = 0; k < 30; k++) {
        theta[k] = calloc(dimension_pb + 1, sizeof (double));
        nb_occ_ki[k] = calloc(dimension_pb + 1, sizeof (double));
    }

    for (int i = 0; i < TAILLE_ENTRAINEMENT; i++) {
        for (int j = 0; j < list_entr->docs[i].taille; j++) {
            int k = list_entr->docs[i].classe;
            nb_occ_ki[k][list_entr->docs[i].mots[j].indice] += (double) list_entr->docs[i].mots[j].occurrence;
            nb_tot_k[k] += (double) list_entr->docs[i].mots[j].occurrence;
        }
    }

    for (int k = 1; k < 30; k++) {
        for (int i = 1; i < dimension_pb + 1; i++) {
            theta[k][i] = (nb_occ_ki[k][i] + 1) / (nb_tot_k[k] + dimension_pb);
        }
    }

    multi.theta = theta;
    multi.pi = pi;

    free(nb_classe);
    free(nb_occ_ki);
    free(nb_tot_k);
    return multi;
}

struct Param_Bernoulli entrainement_Bernoulli(struct liste_documents* list_entr, long dimension_pb) {
    struct Param_Bernoulli ber;

    //nb_classe[k]= nb de docs de la liste de docs de l'entrainement dans le classe k
    double* nb_classe = calloc(30, sizeof (double));
    nombre_docs_par_classe(nb_classe, list_entr);
    double* pi = malloc(30 * sizeof (double));
    for (int k = 1; k < 30; k++) {
        pi[k] = (double) (nb_classe[k] / TAILLE_ENTRAINEMENT);
    }

    double** theta;
    theta = calloc(30, sizeof (double*));
    double** dfid;
    dfid = calloc(30, sizeof (double*));

    for (int k = 0; k < 30; k++) {
        theta[k] = calloc(dimension_pb + 1, sizeof (double));
        dfid[k] = calloc(dimension_pb + 1, sizeof (double));
    }

    for (int i = 0; i < TAILLE_ENTRAINEMENT; i++) {
        for (int j = 0; j < list_entr->docs[i].taille; j++) {
            int k = list_entr->docs[i].classe;
            dfid[k][list_entr->docs[i].mots[j].indice]++;
        }
    }

    for (int k = 1; k < 30; k++) {
        for (int i = 1; i < dimension_pb + 1; i++) {
            theta[k][i] = (1 + dfid[k][i]) / (2 + nb_classe[k]);
        }
    }

    ber.theta = theta;
    ber.pi = pi;
    free(nb_classe);
    free(dfid);
    return ber;
}

void calcul_argmax_multi(long* tab_prediction, struct Param_Multinomial param_multi,
        struct liste_documents* list_test) {
    double* pi = param_multi.pi;
    double **theta = param_multi.theta;

    for (int i = 0; i < TAILLE_TEST; i++) {
        double max = -10000000000;
        for (int k = 1; k < 30; k++) {
            double res = 0;
            res += log((double) pi[k]);
            for (int j = 0; j < list_test->docs[i].taille; j++) {
                res += (double) (list_test->docs[i].mots[j].occurrence)
                        * log((double) theta[k][list_test->docs[i].mots[j].indice]);
            }
            if (res >= max) {
                max = res;
                tab_prediction[i] = k;
            }
        }
    }

}

void calcul_argmax_bernoulli(long* tab_prediction, struct Param_Bernoulli param_bernoulli,
        struct liste_documents* list_test, long dimension_pb) {
    double* pi = param_bernoulli.pi;
    double **theta = param_bernoulli.theta;
    double **LogTheta;
    LogTheta = calloc(30, sizeof (double*));
    for (int k = 0; k < 30; k++) {
        LogTheta[k] = calloc(dimension_pb + 1, sizeof (double));
    }
    double **LogThetaMoins;
    LogThetaMoins = calloc(30, sizeof (double*));
    for (int k = 0; k < 30; k++) {
        LogThetaMoins[k] = calloc(dimension_pb + 1, sizeof (double));
    }

    for (int k = 1; k < 30; k++) {
        for (int l = 0; l < dimension_pb; l++) {
            LogTheta[k][l] = log(theta[k][l]);
            LogThetaMoins[k][l] = log(1 - theta[k][l]);
        }
    }

    for (int i = 0; i < TAILLE_TEST; i++) {
        double max = -10000000000;
        for (int k = 1; k < 30; k++) {
            double res = 0;
            res += log((double) pi[k]);
            int j = 0;
            int indice = list_test->docs[i].mots[j].indice;

            for (int l = 0; l < dimension_pb; l++) {
                if (l == indice) {
                    res += LogTheta[k][l];
                    j++;
                    indice = list_test->docs[i].mots[j].indice;
                } else {
                    res += LogThetaMoins[k][l];
                }
            }
            if (res >= max) {
                max = res;
                tab_prediction[i] = k;
            }
        }
    }

}

double calcul_taux_classification(long* tab_prediction, struct liste_documents* list_test) {
    double taux;
    int nb_bien_classe = 0;
    for (int i = 0; i < TAILLE_TEST; i++) {
        if (tab_prediction[i] == list_test->docs[i].classe) {
            nb_bien_classe++;
        }
    }
    taux = (double) nb_bien_classe / (double) TAILLE_TEST;
    return taux;
}