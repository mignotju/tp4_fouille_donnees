/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "parser.h"
#include "sortie_excel.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("Création des données permettant de créer un histogramme des nombres de documents par classe.\n");
    struct liste_documents list_docs;
    list_docs.taille = NOMBRE_DOCS;
    list_docs.docs = malloc(NOMBRE_DOCS * sizeof (struct document));

    parser_doc("BaseReuters-29", &list_docs);

    double* tab_classes = calloc(30, sizeof (double));
    nombre_docs_par_classe(tab_classes, &list_docs);

    ecriture_fichier_excel(tab_classes, "excel/histogramme.txt", 1, 1, 30);

    free(list_docs.docs);
    free(tab_classes);

    return (EXIT_SUCCESS);
}

