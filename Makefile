MULTI=multi
BERN=bern
HISTO=histo
SRC= parser.c fonction_classification.c histogramme_classes.c main_multi.c main_bernoulli.c sortie_excel.c
HEAD=parser.h fonction_classification.h sortie_excel.h
CFLAGS=-Wall -Wextra -g -std=c99
LM= -lm
CC=gcc

all : $(MULTI) $(BERN) $(HISTO)

multi : parser.o fonction_classification.o main_multi.o sortie_excel.o
	$(CC) -o $@ $(CFLAGS) $^ $(LM)

bern : parser.o fonction_classification.o main_bernoulli.o sortie_excel.o
	$(CC) -o $@ $(CFLAGS) $^ $(LM)

histo : parser.o histogramme_classes.o sortie_excel.o
	$(CC) -o $@ $(CFLAGS) $^ $(LM)

%.o: %.c 
	$(CC) -o $@ -c $(CFLAGS) $<

clean :
	rm -f *.o core
	rm -f *~
	rm -f excel/*
	rm -f $(MULTI)
	rm -f $(BERN)
	rm -f $(HISTO)
