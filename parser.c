/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "parser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h> 

int calcul_nbre_mots_dans_un_doc(int j, char* ligne) {
    int nb_mots = 0;
    int i = j;
    if (ligne[i] == '\n') {
        return 0;
    }
    while (ligne[i] != '\n') {
        i++;
        while (ligne[i] != ' ' && ligne[i] != '\n') {
            i++;
        }
        nb_mots++;
    }
    nb_mots--;
    return nb_mots;
}

long calcul_indice_mot(int* i, char* ligne) {
    int j = *i;
    long indice = 0;
    while (ligne[j] != ':') {
        indice = indice * 10 + ligne[j] - 48;
        j++;
    }
    *i = j;
    return indice;
}

long calcul_occ_mot(int* i, char* ligne) {
    long valeur = 0;
    int j = *i;
    while (ligne[j] != ' ' && ligne[j] != '\n') {
        valeur = valeur * 10 + ligne[j] - 48;
        j++;
    }
    *i = j;
    return valeur;
}

void parser_doc(char* texte, struct liste_documents* list_docs) {

    FILE* fichier = NULL;
    fichier = fopen(texte, "r");

    if (fichier == NULL) {
        perror("Probleme d'ouverture du fichier\n");
    } else {

        char ligne[TAILLE_MAX_LIGNE] = "";
        int i = 0;
        int numeroDoc = 0;
        int classe = 0;
        int nb_mots = 0;


        while (fgets(ligne, TAILLE_MAX_LIGNE, fichier) != NULL) {

            // printf("%s \n", ligne);
            while (ligne[i] != ' ') {
                classe = classe * 10 + ligne[i] - 48;
                i++;
            }

            (*list_docs).docs[numeroDoc].classe = classe;

            nb_mots = calcul_nbre_mots_dans_un_doc(i, ligne);

            (*list_docs).docs[numeroDoc].mots = malloc(nb_mots * sizeof (struct mot));
            (*list_docs).docs[numeroDoc].taille = nb_mots;

            //printf("i : %i \n", i);

            for (long j = 0; j < nb_mots; j++) {
                i++;
                (*list_docs).docs[numeroDoc].mots[j].indice = calcul_indice_mot(&i, ligne);
                i++;
                (*list_docs).docs[numeroDoc].mots[j].occurrence = calcul_occ_mot(&i, ligne);
            }



            i = 0;
            classe = 0;
            numeroDoc++;
            nb_mots = 0;
        }
    }

    fclose(fichier);
}

long trouve_dimension(struct liste_documents* list_docs) {
    long indice_cour = 0;
    for (int i = 0; i < list_docs->taille; i++) {
        int taille = list_docs->docs[i].taille;
        indice_cour = max(indice_cour, list_docs->docs[i].mots[taille - 1].indice);
    }
    return indice_cour;
}

void nombre_docs_par_classe(double* tab, struct liste_documents* list_docs) {
    for (int i = 0; i < list_docs->taille; i++) {
        tab[list_docs->docs[i].classe] = tab[list_docs->docs[i].classe] + 1;
    }
}

void scinder(struct liste_documents* list_depart, struct liste_documents* list_docs_entr,
        struct liste_documents* list_docs_test) {

    int k;
    int *tab = (int*) calloc(sizeof (int), NOMBRE_DOCS);
    for (k = 0; k < NOMBRE_DOCS; k++)
        tab[k] = k;

    //Ensuite on definit un tableau de pointeurs vers chacun de ces elements int 
    int **ptr = (int**) calloc(sizeof (int*), NOMBRE_DOCS);
    for (k = 0; k < NOMBRE_DOCS; k++)
        ptr[k] = &tab[k];

    int NN = NOMBRE_DOCS - 1;

    int *tmp_ptr;
    int random_index;

    for (k = 0; k < NOMBRE_DOCS; k++) {
        //On genere un nombre aleatoire entre 0 et NN,
        random_index = (int) (rand() / (float) RAND_MAX * (NN));

        //Ensuite, on fait une permutation entre le pointeur tire et le dernier
        //element du tableau de pointeurs
        tmp_ptr = ptr[NOMBRE_DOCS - 1 - k];
        ptr[NOMBRE_DOCS - 1 - k] = ptr[random_index];
        ptr[random_index] = tmp_ptr;

        //On decremente N--, de sorte que le dernier element du tableau de pointeur
        //(qui est maintenant celui qui vient d'etre tire) ne puisse pas etre a nouveau selectionne.
        NN--;
    }

    // Les 52500 premiers indices pointés correspondront a la base d'entrainement
    // et les 18203 indices pointés suivants à la base de test
    for (int i = 0; i < TAILLE_ENTRAINEMENT; i++) {
        memcpy(&(list_docs_entr->docs[i]), &(list_depart->docs[*(ptr[i])]), sizeof (struct document));
    }

    for (int i = 0; i < TAILLE_TEST; i++) {
        memcpy(&(list_docs_test->docs[i]), &(list_depart->docs[*(ptr[TAILLE_ENTRAINEMENT + i])]), sizeof (struct document));
    }

    free(tab);
    free(ptr);
}

