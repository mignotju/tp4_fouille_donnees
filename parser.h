/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   parser.h
 * Author: mignotju
 *
 * Created on April 16, 2016, 2:07 PM
 */

#ifndef PARSER_H
#define PARSER_H

#define NOMBRE_DOCS 70703
#define TAILLE_MAX_LIGNE 100000
#define TAILLE_MAX_INDICE 100
#define TAILLE_MAX_OCC 10

#define max(a,b) (a > b) ? a : b

// ":" est représenté par 58 en ASCII
// " " est représenté par 32 en ASCII

#define TAILLE_ENTRAINEMENT 52500
#define TAILLE_TEST 18203

struct mot {
    long indice;
    long occurrence;
};

struct document {
    int classe;
    int taille;
    struct mot* mots;
};

struct liste_documents {
    struct document* docs;
    int taille;
};


int calcul_nbre_mots_dans_un_doc(int j, char* ligne);
long calcul_indice_mot(int* i, char* ligne);
long calcul_occ_mot(int* i, char* ligne);
void parser_doc(char* texte, struct liste_documents* list_docs);
long trouve_dimension(struct liste_documents* list_docs);
void nombre_docs_par_classe(double* tab,struct liste_documents* list_docs);
void scinder(struct liste_documents* list_depart, struct liste_documents* list_entr,
        struct liste_documents* list_test);
void suppression_liste(struct liste_documents liste);


#endif /* PARSER_H */

