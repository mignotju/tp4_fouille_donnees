/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fonction_classification.h
 * Author: mignotju
 *
 * Created on April 17, 2016, 4:11 PM
 */
#include "parser.h"

#ifndef FONCTION_CLASSIFICATION_H
#define FONCTION_CLASSIFICATION_H

struct Param_Bernoulli {
    double** theta; // theta[k][i] est la prob de presence du terme ti dans la classe k
    double* pi; // pi[k]=nombre de docs de classe k dans ensemble apprentissage/ nombre de docs dans ensemble appretissage
};

struct Param_Multinomial {
    double** theta;
    double* pi;
};

struct Param_Multinomial entrainement_Multinomial(struct liste_documents* list_entr, long dimension_pb);
struct Param_Bernoulli entrainement_Bernoulli(struct liste_documents* list_entr, long dimension_pb);

void calcul_argmax_multi(long* tab_prediction, struct Param_Multinomial param_multi,
        struct liste_documents* list_test);
void calcul_argmax_bernoulli(long* tab_prediction, struct Param_Bernoulli param_bernoulli,
        struct liste_documents* list_test, long dimension_pb);
double calcul_taux_classification(long* tab_prediction, struct liste_documents* list_test);

#endif /* FONCTION_CLASSIFICATION_H */

