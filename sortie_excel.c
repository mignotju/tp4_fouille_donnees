/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include "sortie_excel.h"

void ecriture_fichier_excel(double* tab, char* nom_fichier, int cast, int deb, int fin) {
    FILE* fichier = fopen(nom_fichier, "w");
    if (fichier != NULL) {
        for (int i = deb; i < fin; i++) {
            fprintf(fichier, "%i", i);
            fputc(' ', fichier);
            if (cast)
                fprintf(fichier, "%i", (int) tab[i]);
            else
                fprintf(fichier, "%f", tab[i]);
            fputc('\n', fichier);
        }
        fclose(fichier);
    } else {
        perror("Problème ouverture de fichier\n");
    }
}

